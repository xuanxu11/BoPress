# -*- coding: utf-8 -*-


from sqlalchemy import Column, String, PickleType, \
    DateTime, Integer, BigInteger, Text, PrimaryKeyConstraint, UniqueConstraint

from bopress.orm import Entity, many_to_one

__author__ = 'yezang'


# Options

class Options(Entity):
    option_id = Column(String(22), nullable=False, primary_key=True)
    option_name = Column(String(255), unique=True, nullable=False, default="")
    option_value = Column(PickleType, nullable=False)
    # yes or no
    autoload = Column(String(20), nullable=False, default="yes")


# Users And Role And Permission
class Users(Entity):
    user_id = Column(String(22), nullable=False, primary_key=True,
                     info={'label': '用户主键', 'description': '用户主键'})
    user_login = Column(String(100), index=True, nullable=False, unique=True,
                        info={'label': '用户名', 'description': '登录名称'})
    user_pass = Column(String(255), nullable=False,
                       info={'label': '密码', 'description': '登录密码'})
    user_nicename = Column(String(50), index=True, nullable=False, default='',
                           info={'label': '个性域名', 'description': '用户空间名称、通常以英文命名'})
    user_email = Column(String(100), nullable=False, default='',
                        info={'label': '邮箱', 'description': '用户邮箱、可作为登录名称'})
    user_mobile_phone = Column(String(20), nullable=False, default='',
                               info={'label': '手机', 'description': '用户移动电话、可作为登录名称'})
    user_url = Column(String(100), nullable=False, default='',
                      info={'label': '主页', 'description': '用户主页'})
    # 注册时间
    user_registered = Column(DateTime, nullable=False,
                             info={'label': '注册时间', 'description': '用户注册时间'})
    user_activation_key = Column(String(22), nullable=False, default='',
                                 info={'label': '激活码', 'description': '用来作为邮件、电话激活'})
    # 0 未激活 1 已激活
    user_status = Column(Integer, nullable=False, default=0,
                         info={'label': '状态', 'choices': [(0, '未激活'), (1, '激活')],
                               'description': '用户状态、激活用户才可以登录系统'})
    display_name = Column(String(255), nullable=False, default='',
                          info={'label': '昵称', 'description': '用户昵称'})


@many_to_one(Users)
class UserMeta(Entity):
    meta_id = Column(String(22), nullable=False, primary_key=True)
    meta_key = Column(String(255), index=True, nullable=False, default='')
    meta_value = Column(PickleType, nullable=False)


@many_to_one(Users)
class UserRole(Entity):
    role_id = Column(String(22), nullable=False, primary_key=True)
    role_name = Column(String(50), index=True, nullable=False)


@many_to_one(Users)
class UserObjectPermissions(Entity):
    user_object_permission_id = Column(String(22), nullable=False, primary_key=True)
    cls_name = Column(String(255), index=True, nullable=False)
    perm = Column(String(100), nullable=False)


@many_to_one(UserObjectPermissions)
class ObjectPermissions(Entity):
    object_permission_id = Column(String(22), nullable=False, primary_key=True)
    object_id = Column(String(22), nullable=False, index=True)


# Taxonomy
class Terms(Entity):
    term_id = Column(String(22), nullable=False, primary_key=True)
    name = Column(String(200), index=True, nullable=False, default='')
    slug = Column(String(200), unique=True, nullable=False, default='')
    term_group = Column(BigInteger, nullable=False, default=0)


class TermTaxonomy(Entity):
    term_taxonomy_id = Column(String(22), nullable=False, primary_key=True)
    term_id = Column(String(22), nullable=False, default='')
    taxonomy = Column(String(32), index=True, nullable=False, default='')
    description = Column(Text(), nullable=False)
    parent_id = Column(String(22), nullable=False, default='')
    position = Column(Integer(), nullable=False, default=0)
    count = Column(BigInteger(), nullable=False, default=0)
    UniqueConstraint(term_id, taxonomy)


@many_to_one(TermTaxonomy)
class TermTaxonomyMeta(Entity):
    meta_id = Column(String(22), nullable=False, primary_key=True)
    meta_key = Column(String(255), index=True, nullable=False, default='')
    meta_value = Column(PickleType, nullable=False)


class TermRelationships(Entity):
    object_id = Column(String(22), nullable=False, default='')
    term_taxonomy_id = Column(String(22), index=True, nullable=False)
    cls_name = Column(String(255), index=True, nullable=False, default='')
    term_order = Column(Integer, nullable=False, default=0)
    PrimaryKeyConstraint(term_taxonomy_id, object_id, cls_name)


class Attachment(Entity):
    attachment_id = Column(String(22), nullable=False, primary_key=True)
    origin_name = Column(String(255), index=True, nullable=False, default='')
    mime_type = Column(String(50), nullable=False, default='')
    size = Column(BigInteger(), nullable=False, default=0)
    relative_path = Column(String(255), nullable=False, default='')
    save_folder = Column(String(255), nullable=False, default='')
