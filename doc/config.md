# 配置 #

BoPress 是全插件式结构，毫无例外，一切从插件开始，配置也是。`bo_settings`扩展点扩展一次就可以了，可在`plugins/bocore/meta.py`文件内修改。此扩展点在系统启动时第一个加载。
	
	from bopress.hook import add_action

    def bo_settings(settings):
	    settings.DEBUG = True
		settings.CACHE_MODE = "simple"
		settings.TABLE_NAME_PREFIX = "bo_"
	    # settings.DB_CONNECT_URI = "sqlite:///bopress.sqlite3"
	    settings.DB_CONNECT_URI = "mysql+pymysql://root:@127.0.0.1:3306/bopress?charset=utf8"


	add_action("bo_settings", bo_settings)

settings属性请参考`bopress.settings`模块。
- `SESSION_TIMEOUT` Session超时(秒)
- `DEBUG` 是否开启调试模式
- `CACHE_MODE` 缓存方式，目前仅支持`simple`、`file`，`memcache`及`redis`后续增加。
- `TABLE_NAME_PREFIX` 数据表名前缀
- `DB_CONNECT_URI` 数据库连接，不同数据库连接方式请参阅[http://docs.sqlalchemy.org/en/latest/dialects/index.html](http://docs.sqlalchemy.org/en/latest/dialects/index.html)

Tornado 服务器配置
	
	from bopress.hook import add_action

    def tornado_server(app):
		pass
	add_action("bo_tornado_server", tornado_server)

`app`修改请参阅`tornado.web.Application`类