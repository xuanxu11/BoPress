function BoTree(elm_id, taxonomy, nodes, api_url) {
    var data = {
        element_id: elm_id,
        taxonomy: taxonomy,
        nodes: nodes,
        api_url: api_url
    };
    $("#" + elm_id).data('bo_term_taxonomy_tree', data);
}

BoTree.prototype.settings = function () {
    var self = this;
    return {
        view: {
            addHoverDom: self.addHoverDom,
            removeHoverDom: self.removeHoverDom,
            selectedMulti: false
        },
        edit: {
            enable: true,
            editNameSelectAll: true,
            showRemoveBtn: self.showRemoveBtn,
            showRenameBtn: self.showRenameBtn
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        callback: {
            beforeDrag: self.beforeDrag,
            beforeEditName: self.beforeEditName,
            beforeRemove: self.beforeRemove,
            beforeRename: self.beforeRename,
            onRemove: self.onRemove,
            onRename: self.onRename
        }
    };
};

BoTree.prototype.beforeDrag = function (treeId, treeNodes) {
    return false;
};

BoTree.prototype.beforeEditName = function (treeId, treeNode) {
    var data = $("#" + treeId).data('bo_term_taxonomy_tree');
    document.getElementById(treeId + '_form').reset();
    $('#' + treeId + '_edit_dialog_action_name').html('编辑 ' + treeNode.name + ' ');
    $('#' + treeId + '_term_taxonomy_id').val(treeNode.id);
    var zTree = $.fn.zTree.getZTreeObj(treeId);
    zTree.selectNode(treeNode);
    $('#' + treeId + '_edit_modal').modal({show: true, keyboard: true});
    $.post(data['api_url'], bo_xsrf({action: 'get', term_taxonomy_id: treeNode.id, taxonomy: data['taxonomy']}), function (rsp) {
        if (rsp.success && rsp.message == "200") {
            var tt = rsp.data;
            if (tt.length == 1) {
                tt = tt[0];
                $("#" + treeId + "_parent_id").val(tt[1]);
                $("#" + treeId + "_tt_name").val(tt[2]);
                $("#" + treeId + "_tt_slug").val(tt[3]);
                $("#" + treeId + "_tt_description").val(tt[4]);
                $("#" + treeId + "_tt_position").val(tt[5]);
                $("#" + treeId + "_save_action").val('update');
            }
        }
    }, "json");
    return false;
};

BoTree.prototype.beforeRemove = function (treeId, treeNode) {
    if (treeNode.isParent) {
        alert('只能先删除子分类!');
        return;
    }
    var zTree = $.fn.zTree.getZTreeObj(treeId);
    zTree.selectNode(treeNode);
    var b = confirm("确认删除 " + treeNode.name + " ？");
    var c = false;
    if (b) {
        var data = $("#" + treeId).data('bo_term_taxonomy_tree');
        $.ajax({
            type: "POST",
            async: false,
            url: data['api_url'],
            data: bo_xsrf({action: 'delete', taxonomy: data['taxonomy'], term_taxonomy_id: treeNode.id}),
            success: function (rsp) {
                if (rsp.success) {
                    c = true;
                } else {
                    alert(rsp.message);
                }
            },
            dataType: 'json'
        });
    }
    return c;
};

BoTree.prototype.onRemove = function (e, treeId, treeNode) {
};

BoTree.prototype.beforeRename = function (treeId, treeNode, newName, isCancel) {
    if (newName.length == 0) {
        setTimeout(function () {
            var zTree = $.fn.zTree.getZTreeObj(treeId);
            zTree.cancelEditName();
            alert("节点名称不能为空.");
        }, 0);
        return false;
    }
    return true;
};

BoTree.prototype.onRename = function (e, treeId, treeNode, isCancel) {
};

BoTree.prototype.showRemoveBtn = function (treeId, treeNode) {
    if (treeNode.isParent) {
        return false;
    } else if (treeNode.id == '') {
        return false;
    }
    return true;
};

BoTree.prototype.showRenameBtn = function (treeId, treeNode) {
    return !treeNode.id == '';
};
var newCount = 0;
BoTree.prototype.addHoverDom = function (treeId, treeNode) {
    var sObj = $("#" + treeNode.tId + "_span");
    var btn = $("#addBtn_" + treeNode.tId);
    if (treeNode.editNameFlag || btn.length > 0) return;
    var addStr = "<span class='button add' id='addBtn_" + treeNode.tId +
        "' title='add node' onfocus='this.blur();'></span>";
    sObj.after(addStr);
    if (btn) btn.bind("click", function () {
        var zTree = $.fn.zTree.getZTreeObj(treeId);
        zTree.selectNode(treeNode);
        document.getElementById(treeId + '_form').reset();
        $('#' + treeId + '_term_taxonomy_id').val('');
        $('#' + treeId + '_edit_dialog_action_name').html('在 ' + treeNode.name + ' 下面新增');
        $('#' + treeId + '_parent_id').val(treeNode.id);
        $('#' + treeId + '_save_action').val('create');
        $('#' + treeId + '_edit_modal').modal({show: true, keyboard: true});
        return false;
    });
};

BoTree.prototype.removeHoverDom = function (treeId, treeNode) {
    $("#addBtn_" + treeNode.tId).unbind().remove();
};

BoTree.prototype.selectAll = function () {
    var zTree = $.fn.zTree.getZTreeObj(treeId);
    zTree.setting.edit.editNameSelectAll = $("#selectAll").attr("checked");
};