# -*- coding: utf-8 -*-

import os
import sys
import webbrowser

import tornado.web
from tornado import httpserver
from tornado import ioloop, autoreload
from tornado import options

from bopress import coreplugins, ui, __version__
from bopress import settings
from bopress.handlers import urls
from bopress.hook import Hooks, do_action
from bopress.log import Logger
from bopress.orm import SessionFactory
from bopress.utils import Utils

__author__ = 'yezang'

settings.BASE_DIR = os.path.dirname(os.path.abspath(sys.argv[0]))
options.define('port', 9090, int, 'Tornado Server Listen Port.')
settings.PLUGINS_ROOT = os.path.join(settings.BASE_DIR, "plugins")
Utils.mkdirs(settings.PLUGINS_ROOT)
sys.path.append(settings.PLUGINS_ROOT)
Hooks.load(settings.PLUGINS_ROOT, coreplugins.load)
do_action("bo_settings", settings)
Logger.init()
SessionFactory.create_tables()

# 插件静态路径路由映射
static_paths = Hooks.static_paths()
for static_path in static_paths:
    urls.append(tornado.web.url(r"/bopress/plugin/static/{0}/(.*)".format(static_paths[static_path]),
                                tornado.web.StaticFileHandler,
                                {"path": os.path.join(settings.PLUGINS_ROOT, static_path)}))

do_action("bo_urlmapping", urls)
ui_modules = list()
ui_modules.append(ui)
do_action("bo_uimodules", ui_modules)
if __name__ == '__main__':
    # BoPress.exe --port=9090
    options.parse_command_line()
    app = tornado.web.Application(
        debug=settings.DEBUG,
        autoreload=True,
        handlers=urls,
        static_path=os.path.join(settings.BASE_DIR, "static"),
        template_path=settings.PLUGINS_ROOT,
        ui_modules=ui_modules,
        cookie_secret=settings.COOKIE_SECRET,
        xsrf_cookies=settings.XSRF_COOKIES,
    )
    coreplugins.init_data()
    do_action("bo_tornado_server", app)
    print("BoPress {0}".format(__version__))
    print("Tornado %s running %i.." % (tornado.version, options.options.port))
    http_server = httpserver.HTTPServer(app, max_buffer_size=1024*1024)
    # http_server.listen(options.options.port, "127.0.0.1")
    http_server.listen(options.options.port)
    if settings.USE_BROWSER:
        webbrowser.open_new_tab("http://127.0.0.1:%i" % options.options.port)
    # 插件重加载、服务器也重加载、当前新加路由需要重启服务器才能生效、希望后续有更好解决方案
    # 同时服务器重启功能需要打包方式运行才有效果，可能是有一个独立的进程在运行
    watch_file = os.path.join(settings.BASE_DIR, "cache", "watch.txt")
    autoreload.watch(watch_file)
    ioloop.IOLoop.instance().start()
